<?php

namespace App\Entity;

use App\User;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase {
  public function testTellName() {
    $user = new User(21, "Alex");
    $this->assertEquals("My name is Alex.", $user->tellName());
  }

  public function testTellAge() {
    $user = new User(25, "John");
    $this->assertEquals("I am 25 years old.", $user->tellAge());
  }

  public function testAddFavoriteMovie() {
    $user = new User(23, "Yoann");
    $this->assertEmpty($user->favorite_movies);
    $this->assertTrue($user->addFavoriteMovie("Je sais pas"));
    $this->assertEquals(["Je sais pas"], $user->favorite_movies);
  }

  public function testRemoveFavoriteMovie() {
    $user = new User(25, "John");
    $user->addFavoriteMovie("Projet Almanac");
    $this->assertNotEmpty($user->favorite_movies);
    $this->assertTrue($user->removeFavoriteMovie("Projet Almanac"));
    $this->assertEquals([], $user->favorite_movies);
  }

  public function testRemoveUnknownFavoriteMovie() {
    $user = new User(25, "John");
    $this->expectException(InvalidArgumentException::class);
    $user->removeFavoriteMovie("The Godfather");
  }
}
